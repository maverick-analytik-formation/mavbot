import os
import uuid
from azure.core.exceptions import ResourceNotFoundError
from azure.storage.blob import (
    BlobServiceClient,
    BlobClient,
    ContainerClient,
)


class AzureBlobStorageApi:
    """
    Usage
    -----

    """

    def __init__(self, connection_string):

        self.connection_string = connection_string
        self.__blob_service_client = None
        self.blob_client = None
        self.container_list = None
        self.current_container_name = None
        self.container_client = None

    @property
    def blob_service_client(self):
        if self.__blob_service_client is None:
            self._init_blob_service_client()

        return self.__blob_service_client

    def _init_blob_service_client(self):
        self.__blob_service_client = BlobServiceClient.from_connection_string(
            self.connection_string
        )

    def _get_container_client(self, container_name):
        """
        """
        # if same container name and client already created, use it
        if (
                self.current_container_name == container_name
                and
                self.container_client is not None
        ):
            return self.container_client

        # else
        self.container_client = self.blob_service_client.get_container_client(
            container_name
        )

        return self.container_client

    def _get_blob_client(self, container_name, blob_name):
        """
        """
        self.blob_client = self.blob_service_client.get_blob_client(
            container_name,
            blob_name
        )

        return self.blob_client

    def list_containers(self, name_starts_with=None):
        """
        """
        container_list = self.blob_service_client.list_containers(
            name_starts_with=name_starts_with,
            include_metadata=False,
        )
        print("{:<30s}{:<20s}".format("Container's name",
                                      "Container's metadata"))
        for container in container_list:
            print("{:<30s}{:<20s}".format(str(container['name']),
                                          str(container['metadata'])))

        return

    def list_blobs_in_container(self, container_name):
        """
        """
        container_client = self._get_container_client(container_name)

        blob_list = container_client.list_blobs()

        print(f"blobs in container {container_name}:\n")
        for blob in blob_list:
            print(f"\t{blob.name}")

    def upload_file_in_container(self, container_name, file_path):
        """
        """
        _, blob_name = os.path.split(os.path.abspath(file_path))

        blob_client = self._get_blob_client(container_name, blob_name)

        print(f"Uploading {file_path} to Azure Blob Storage")

        with open(file_path, "rb") as data:
            blob_client.upload_blob(data)

        print("Upload sucessful")

        return

    def download_blob_from_container(self,
                                     container_name,
                                     blob_name,
                                     download_dir_path):
        """
        """
        blob_client = self._get_blob_client(container_name, blob_name)

        # verify if download directory path exist
        # create it otherwise
        download_dir_abspath = os.path.abspath(download_dir_path)
        if not os.path.isdir(download_dir_abspath):
            print(f"Download directory created\n->{download_dir_abspath}")
            os.makedirs(os.path.abspath(download_dir_path))

        try:
            print(f"Starting download of blob {blob_name}")
            download_file_path = os.path.join(download_dir_path, blob_name)

            with open(download_file_path, 'wb') as download_file:
                download_file.write(blob_client.download_blob().readall())
            print("Download successful")
        except ResourceNotFoundError:
            print("No blob found with that name")
            print("Download failed")

        return

