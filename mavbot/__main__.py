from mavbot.storage import AzureBlobStorageApi
import click


def read_key_file(key_file):
    # read file content
    with open(key_file, 'r') as f:
        connection_string = f.readline().strip()
    return connection_string


@click.group()
def cli():
    pass


@cli.command()
@click.option('--key-file',
              default=r'./connection_string.txt',
              help='path to key file used to connect to Azure blob service')
@click.argument('container_name')
@click.argument('file_name')
def upload(key_file, container_name, file_name):
    connection_string = read_key_file(key_file)

    api = AzureBlobStorageApi(connection_string)
    api.upload_file_in_container(container_name,
                                 file_name)
    click.echo(f'uploading {file_name}')


@cli.command()
@click.option('--key-file',
              default=r'./connection_string.txt',
              help='path to key file used to connect to Azure blob service')
@click.option('--download-target-directory',
              default=r'./',
              help='path on disc where the blob will be written')
@click.argument('blob_name')
@click.argument('container_name')
def download(key_file,
             download_target_directory,
             blob_name,
             container_name):
    connection_string = read_key_file(key_file)

    api = AzureBlobStorageApi(connection_string)

    api.download_blob_from_container(container_name,
                                     blob_name,
                                     download_dir_path=download_target_directory)


@cli.command()
@click.option('--key-file',
              default=r'./connection_string.txt',
              help='path to key file used to connect to Azure blob service')
def list_containers(key_file):
    connection_string = read_key_file(key_file)

    api = AzureBlobStorageApi(connection_string)

    api.list_containers()


@cli.command()
@click.option('--key-file',
              default=r'./connection_string.txt',
              help='path to key file used to connect to Azure blob service')
@click.argument('container_name')
def list_blobs(key_file, container_name):
    connection_string = read_key_file(key_file)

    api = AzureBlobStorageApi(connection_string)

    api.list_blobs_in_container(container_name)


if __name__ == "__main__":
    cli()
